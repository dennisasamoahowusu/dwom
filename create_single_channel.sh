#!/bin/bash

SOURCE=/home/ubuntu/coding/kaldi/egs/dwom/resources/audio/*
DEST=/home/ubuntu/coding/kaldi/egs/dwom/resources/audio_single_channel

for file in $SOURCE
do
  echo "Processing $file ..."
  filename="${file##*/}"
  sox $file $DEST/$filename remix 1
done
